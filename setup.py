from setuptools import setup, find_packages

VERSION = '0.0.1'
DESCRIPTION = 'An API implentation of the cliBattleship project'

setup(
    name='cliBattleship-api',
    version=VERSION,
    author='Tyson Van Patten',
    author_email='tyson@descriptdata.com',
    url='https://gitlab.com/tvanpat/clibattleship-api',
    description=DESCRIPTION,
    packages=find_packages(),
    install_requires=['cliBattleship',
                      'pymongo',
                      'PyYAML',
                      'python-dotenv']
)
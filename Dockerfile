FROM tiangolo/uvicorn-gunicorn-fastapi

ENV PYTHONUNBUFFERED 1

COPY ./setup.py /usr/src
WORKDIR /usr/src
RUN pip install .

COPY ./src /usr/src

CMD ["uvicorn", "main:app", "--host", "0.0.0.0", "--port", "80"]
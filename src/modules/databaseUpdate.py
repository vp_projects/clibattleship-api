import pymongo
import os
from dotenv import load_dotenv

load_dotenv()
mongo_client = os.getenv('mongo_client')
mongodb = os.getenv('mongodb')
mongocol = os.getenv('mongocol')


def mongoSingleAdd(mongodict):
    myclient = pymongo.MongoClient(f'mongodb:{mongo_client}')
    mydb = myclient[mongodb]
    mycol = mydb[mongocol]
    mycol.insert_one(mongodict)
    myclient.close()


def mongoMultiAdd(mongolist):
    myclient = pymongo.MongoClient(f'mongodb:{mongo_client}')
    mydb = myclient[mongodb]
    mycol = mydb[mongocol]
    mycol.insert_many(mongolist)
    myclient.close()


def prepareGameResults(gl, player1, player2):
    gameResult = gl[-1]
    gameWinnerName = gameResult.get('gameWinner')
    if gameWinnerName == player1.name:
        gameWinner = player1
        gameLoser = player2
    else:
        gameWinner = player2
        gameLoser = player1
    return {
        'winningPlayer': gameWinner.name,
        'winningPlayerLevel': gameWinner.computerLevel,
        'winningPlayerMoveCount': gameResult.get('offPlayerMoves'),
        'losingPlayer': gameLoser.name,
        'losingPlayerLevel': gameLoser.computerLevel,
        'gameLog': gl}
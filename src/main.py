from fastapi import FastAPI
from typing import List
from pydantic import BaseModel, Field

import cliBattleship.gameModules.gameBoards as gb
import cliBattleship.gameModules.autoShipPlacement as asp
import cliBattleship.gameModules.playerNumberFunctions as pnf
import cliBattleship.gameModules.players as pl

from modules.databaseUpdate import mongoSingleAdd
from modules.databaseUpdate import mongoMultiAdd
from modules.databaseUpdate import prepareGameResults

# TODO Make Multigame Mode
# TODO add MongoDB connection

app = FastAPI()


class PlayerInfo(BaseModel):
    p1Name: str = 'Player 1'
    p1Level: int = 0
    p2Name: str = 'Player 2'
    p2Level: int = 0


class MultiPlayerInfo(BaseModel):
    games: List


@app.get("/")
async def root():
    return {"message": "Hello World"}


@app.post("/singlegame/")
async def singleGame(playerInfo: PlayerInfo):
    player1 = pl.Player()
    player2 = pl.Player()
    asp.placeAllships(player1)
    asp.placeAllships(player2)
    player1.name = playerInfo.p1Name
    player1.computerLevel = playerInfo.p1Level
    player2.name = playerInfo.p2Name
    player2.computerLevel = playerInfo.p2Level
    gl = pnf.ComputerGameFunction(player1, player2)
    gameDict = prepareGameResults(gl, player1, player2)
    mongoSingleAdd(gameDict)
    return gl[-1]


@app.post("/multigame/")
async def multigame(playerInfo: List[PlayerInfo]):
    gameResults = []
    gameList = []
    for plInfo in playerInfo:
        player1 = pl.Player()
        player2 = pl.Player()
        asp.placeAllships(player1)
        asp.placeAllships(player2)
        player1.name = plInfo.p1Name
        player1.computerLevel = plInfo.p1Level
        player2.name = plInfo.p2Name
        player2.computerLevel = plInfo.p2Level
        gl = pnf.ComputerGameFunction(player1, player2)
        gameResults.append(gl[-1])
        gameList.append(prepareGameResults(gl, player1, player2))
    mongoMultiAdd(gameList)
    return {'results': gameResults}
